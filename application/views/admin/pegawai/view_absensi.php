<div class="" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Lihat Absensi</h3>
      </div>


    </div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">

        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>

        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" name="form1" action="<?php echo base_url($this->uri->segment(1).'/edit-absensi-pegawai/') ?>">
          <?php echo validation_errors()?>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Unit
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php
                $options = array(
                  0 => 'Tidak Ada',
                  1 => 'Dept. Support',
                  2 => 'LPIA',
                  3 => 'LDSM',
                  4 => 'LPP',
                  5 => 'LEKU',
                  6 => 'TK Islam',
                  7 => 'SDIT',
                  8 => 'MI Plus',
                  9 => 'MTS Plus',
                  10 => 'SMPIT',
                  11 => 'SMAIT',
                  12 => 'STAIS',
                  13 => 'Lainnya'
                  );
                  echo form_dropdown('unit',$options,$result['id_unit'],"class='form-control' disabled");
               ?>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" disabled id="birthday" required="required" class="form-control col-md-7 col-xs-12" name="date_absensi" value="<?php echo $result['date_absensi']?>">
            </div>
          </div>
          <div class="table-responsive">
            <table class="table">
              <thead>
                <th>#</th>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Absensi<br />
                  <label><input type="checkbox" name="absensi_all" id="hadir"> Hadir Semua</label>
                </th>
              </thead>
              <tbody>
                <?php
                $i = 1;
                  if($results!=FALSE){
                    foreach ($results as $rows) {
                      ?>
                      <input type="hidden" name="id_pegawai[<?php echo $i-1 ?>]" value="<?php echo $rows->id_pegawai ?>">
                      <input type="hidden" name="id_absensi[<?php echo $i-1 ?>]" value="<?php echo $rows->id_absensi ?>">
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $rows->nama_lengkap ?></td>
                        <td><?php echo $rows->jabatan; ?></td>
                        <td>
                          <label><input type="checkbox" name="absensi[<?php echo $i-1 ?>]" value="1" id="absensi_hadir" <?php if($rows->absensi == 1) echo "checked" ?>> Hadir</label>
                        </td>
                      </tr>
                      <?php
                      $i++;
                    }
                  }
                 ?>
              </tbody>
            </table>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
