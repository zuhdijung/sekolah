
<?php $this->load->view('admin/common/header_manage');?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Data Tahun Ajaran</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="title_right">
      <form method="POST" action="">
      <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right">
          <?php
            $options = array(
                  'tahun_ajaran'=>'Tahun Ajaran',
            );
            echo form_dropdown('by',$options,set_value('by'),"class='form-control'");
          ?>
        </div>
        <div class="input-group top_search">
          <input type="text" name="search" class="form-control" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">Go!</button>
          </span>
        </div>
        </form>
      </div>
    </div>


    <div class="x_content">
    <a href="<?php echo base_url($this->uri->segment(1).'/add-year')?>"><button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</button></a>

      <div class="table-responsive">
        <table id="datatable" class="table table-striped table-bordered">
          <thead>
            <tr class="headings">
              <th class="column-title">Tahun Ajaran</th>
              <th class="column-title">Status</th>
              <th class="column-title no-link last"><span class="nobr">Action</span>
              </th>

            </tr>
          </thead>

          <tbody>
            <tr class="even pointer">
              <?php
            		if($results!=FALSE){
            			foreach ($results as $rows) {
            				?>
            				<tr>
                      <td><?php echo $rows->tahun_ajaran?></td>
                      <td><?php if($rows->status_tahun_ajaran==0) echo "Active"; else echo "Non-Active"; ?></td>
                    <td><?php
                    if($this->session->userdata('role')==1){
                  ?>
                  <a href ="<?php echo base_url($this->uri->segment(1).'/edit-year/'.$rows->id_tahun_ajaran)?>"><i class="glyphicon glyphicon-edit"></i></a> |
                  <a href ="<?php echo base_url($this->uri->segment(1).'/delete-year/'.$rows->id_tahun_ajaran)?>" onclick="return confirm('Are You Sure want to delete?')"><i class="glyphicon glyphicon-trash"></i></a></span>
                  <?php
                }
                  ?>
                  </td>
                    </tr>
            				<?php
            			}
            		}
            	?>
            	<?php
            		echo $links;
            	?>

            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
