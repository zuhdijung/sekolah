<div class="">
  <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-bars"></i> Ubah Tahun Ajaran</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="col-xs-10">
        <form action="" method="post" accept-charset="utf-8" class="form-horizontal form-label-left" id="form_info">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tahun Ajaran
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="tahun_ajaran" required="required" class="form-control col-md-7 col-xs-12" name="tahun_ajaran" value="<?php echo $result['tahun_ajaran']?>">
            </div>
          </div>
          <div class="item form-group">
            <label  class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php $options = array(
                0 => 'Active',
                1 => 'Nonactive'
              );
              echo form_dropdown('status_tahun_ajaran',$options,$result['status_tahun_ajaran'],'class="form-control"');
              ?>
            </div>
          </div>
          <div class="item form-group">
          <label  class="control-label col-md-3 col-sm-3 col-xs-12"></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <button type="submit" class="btn btn-success" id="add-info">Save</button>
            </div>
          </div>
      </form>
      </div>

      <div class="clearfix"></div>

    </div>
  </div>
</div>

<!-- Modal for Add to Info -->
                                <div id="infoModal" class="modal fade" role="dialog">
                                  <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <!-- <div class="modal-header">
                                      </div> -->
                                      <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                                        <h4 class="modal-title text-center"></h4>
                                      <div class="modal-body">
                                           <div id="result"></div>
                                      </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- /.Modal for Add to Cart -->
