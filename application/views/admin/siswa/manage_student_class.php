
<?php $this->load->view('admin/common/header_manage');?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Data Siswa <?php echo $result['nama_kelas'] ?></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="title_right">
      <form method="POST" action="">
      <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right">
          <?php
            $options = array(
                  'nama_siswa'=>'Nama Siswa'
            );
            echo form_dropdown('by',$options,set_value('by'),"class='form-control'");
          ?>
        </div>
        <div class="input-group top_search">
          <input type="text" name="search" class="form-control" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="submit">Go!</button>
          </span>
        </div>
        </form>
      </div>
    </div>


    <div class="x_content">
      <a href="<?php echo base_url($this->uri->segment(1).'/add-student-class/'.$this->uri->segment(3))?>"><button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</button></a>
    <?php
      echo $links;
    ?>
      <div class="table-responsive">
        <table id="datatable" class="table table-striped table-bordered">
          <thead>
            <tr class="headings">
              <th class="column-title">Siswa</th>
              <th class="column-title">NIS</th>
              <th class="column-title">Alamat</th>
              <th class="column-title">No HP</th>
              <th class="column-title no-link last"><span class="nobr">Action</span>
              </th>

            </tr>
          </thead>

          <tbody>
            <tr class="even pointer">
              <?php
            		if($results!=FALSE){
            			foreach ($results as $rows) {
            				?>
            				<tr>
                      <td><?php echo $rows->nama_siswa?></td>
                      <td><?php echo $rows->nis?></td>
                      <td><?php echo $rows->alamat?></td>
                      <td><?php echo $rows->no_hp?></td>
                    <td><?php
                    if($this->session->userdata('role')==1){
                  ?>
                  <a onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')" title="Hapus Data" href ="<?php echo base_url($this->uri->segment(1).'/delete-student-class/'.$rows->id_siswa_kelas)?>"><i class="fa fa-trash"></i></a>
                  <?php
                }
                  ?>
                  </td>
                    </tr>
            				<?php
            			}
            		}
            	?>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
