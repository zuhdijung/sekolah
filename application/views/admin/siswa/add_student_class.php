<div class="">
  <div class="x_panel">
    <div class="x_title">
      <h2><i class="fa fa-bars"></i> Tambah Siswa <?php echo $result['nama_kelas'] ?></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="col-xs-6">
        <?php
        echo validation_errors();
         ?>
        <form action="" method="post" accept-charset="utf-8" class="form-horizontal form-label-left" id="form_info">
          <table class="table">
            <thead>
              <tr>
                <th>NIS Siswa</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <?php
                    $options = array(''=>'Pilih NIS Siswa');
                    if($nis !=FALSE){
                      foreach ($nis as $rows) {
                        $options[$rows->id_siswa] = $rows->nis.' - '.$rows->nama_siswa;
                      }
                    }
                    echo form_dropdown('nis',$options,set_value('nis'),'class="form-control"');
                   ?>
                </td>
              </tr>
            </tbody>
          </table>
          <div class="item form-group">
          <label  class="control-label col-md-3 col-sm-3 col-xs-12"></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <button type="submit" class="btn btn-success" id="add-info">Save</button>
            </div>
          </div>
        </div>
      </form>
      </div>

      <div class="clearfix"></div>

    </div>
  </div>
</div>

<!-- Modal for Add to Info -->
                                <div id="infoModal" class="modal fade" role="dialog">
                                  <div class="modal-dialog">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <!-- <div class="modal-header">
                                      </div> -->
                                      <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                                        <h4 class="modal-title text-center"></h4>
                                      <div class="modal-body">
                                           <div id="result"></div>
                                      </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- /.Modal for Add to Cart -->
