<div class="" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Tambah Absensi Siswa</h3>
      </div>


    </div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">

        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>

        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" name="form1" action="<?php echo base_url($this->uri->segment(1).'/submit-absensi-student/') ?>">
          <?php echo validation_errors()?>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Unit
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <?php
              $options = array(
                '' => 'Pilih Kelas'
                );
              if($kelas!=FALSE){
                foreach ($kelas as $rows) {
                  $options[$rows->id_kelas] = $rows->nama_kelas.' '.$rows->tahun_ajaran;
                }
              }
                echo form_dropdown('id_kelass',$options,$this->uri->segment(3),"class='form-control'disabled ");
               ?>
               <input type="hidden" name="id_kelas" value="<?php echo $this->uri->segment(3); ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tanggal
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" disabled id="birthday" required="required" class="form-control col-md-7 col-xs-12" name="date_absensis" value="<?php echo date('Y-m-d',$this->uri->segment(4))?>">
              <input type="hidden" name="date_absensi" value="<?php echo date('Y-m-d',$this->uri->segment(4))?>">
            </div>
          </div>
          <div class="table-responsive">
            <table class="table">
              <thead>
                <th>#</th>
                <th>Nama</th>
                <th>NIS</th>
                <th>Absensi<br />
                  <label><input type="checkbox" name="absensi_all" id="hadir"> Hadir Semua</label>
                </th>
              </thead>
              <tbody>
                <?php
                $i = 1;
                  if($results!=FALSE){
                    foreach ($results as $rows) {
                      ?>
                      <input type="hidden" name="id_siswa[<?php echo $i-1; ?>]" value="<?php echo $rows->id_siswa ?>">
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $rows->nama_siswa ?></td>
                        <td><?php echo $rows->nis; ?></td>
                        <td>
                          <label><input type="checkbox" name="absensi[<?php echo $i-1; ?>]" value="1" id="absensi_hadir"> Hadir</label>
                        </td>
                      </tr>
                      <?php
                      $i++;
                    }
                  }
                 ?>
              </tbody>
            </table>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
