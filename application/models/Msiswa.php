<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Msiswa extends CI_Model {
	// constrcutor
	function __construct(){
		parent::__construct();
	}
	function totalKehadiranSiswaUnit($date_absensi,$id_kelas){
		$this->db->where('id_kelas',$id_kelas);
		$this->db->where('date_absensi',$date_absensi);
		return $this->db->count_all_results('absensi_siswa');
	}
	function totalKehadiran($date_absensi,$id_kelas){
		$this->db->where('absensi',1);
		$this->db->where('id_kelas',$id_kelas);
		$this->db->where('date_absensi',$date_absensi);
		return $this->db->count_all_results('absensi_siswa');
	}
	function countAbsensiStudent() {
		$this->db->join('kelas','kelas.id_kelas = absensi_siswa.id_kelas');
		$this->db->join('user','user.id_user = absensi_siswa.id_user_insert');
		$this->db->order_by('date_absensi','DESC');
		$this->db->group_by('date_absensi');
		$this->db->group_by('absensi_siswa.id_kelas');
		$query = $this->db->get('absensi_siswa');
		return $query->num_rows();
	}
	function fetchTahunAjaran($limit,$start,$pagenumber) {
    if($pagenumber!="")
      $this->db->limit($limit,($pagenumber*$limit)-$limit);
    else
      $this->db->limit($limit,$start);

		$this->db->order_by('tahun_ajaran','DESC');
		$query = $this->db->get('tahun_ajaran');
		if($query->num_rows()>0){
			return $query->result();
		}
		else return FALSE;
	}
	function fetchKelas($limit,$start,$pagenumber) {
    if($pagenumber!="")
      $this->db->limit($limit,($pagenumber*$limit)-$limit);
    else
      $this->db->limit($limit,$start);

		$this->db->join('tahun_ajaran','tahun_ajaran.id_tahun_ajaran = kelas.id_tahun_ajaran');
		$this->db->order_by('tahun_ajaran','DESC');
		$this->db->order_by('hirarki','ASC');
		$query = $this->db->get('kelas');
		if($query->num_rows()>0){
			return $query->result();
		}
		else return FALSE;
	}
	function fetchKelasAktif() {
		$this->db->where('status_tahun_ajaran',0);
		$this->db->join('tahun_ajaran','tahun_ajaran.id_tahun_ajaran = kelas.id_tahun_ajaran');
		$this->db->order_by('tahun_ajaran','DESC');
		$this->db->order_by('hirarki','ASC');
		$query = $this->db->get('kelas');
		if($query->num_rows()>0){
			return $query->result();
		}
		else return FALSE;
	}
	function fetchAllYear() {
		$this->db->order_by('tahun_ajaran','DESC');
		$query = $this->db->get('tahun_ajaran');
		if($query->num_rows()>0){
			return $query->result();
		}
		else return FALSE;
	}
	function fetchStudentClass($limit,$start,$pagenumber,$id) {
    if($pagenumber!="")
      $this->db->limit($limit,($pagenumber*$limit)-$limit);
    else
      $this->db->limit($limit,$start);

		$this->db->where('siswa_kelas.id_kelas',$id);
		$this->db->join('siswa','siswa_kelas.id_siswa = siswa.id_siswa');
		$this->db->order_by('nama_siswa','ASC');
		$query = $this->db->get('siswa_kelas');
		if($query->num_rows()>0){
			return $query->result();
		}
		else return FALSE;
	}
	function fetchSiswaKelas($id) {
		$this->db->where('siswa_kelas.id_kelas',$id);
		$this->db->join('siswa','siswa_kelas.id_siswa = siswa.id_siswa');
		$this->db->order_by('nama_siswa','ASC');
		$query = $this->db->get('siswa_kelas');
		if($query->num_rows()>0){
			return $query->result();
		}
		else return FALSE;
	}
	function fetchSiswa($limit,$start,$pagenumber) {
    if($pagenumber!="")
      $this->db->limit($limit,($pagenumber*$limit)-$limit);
    else
      $this->db->limit($limit,$start);

		$this->db->join('tahun_ajaran','tahun_ajaran.id_tahun_ajaran = siswa.id_tahun_masuk');
		$this->db->join('user','user.id_user = siswa.id_user_input');
		$this->db->order_by('date_input','DESC');
		$query = $this->db->get('siswa');
		if($query->num_rows()>0){
			return $query->result();
		}
		else return FALSE;
	}
	function fetchAbsensi($limit,$start,$pagenumber) {

		if($pagenumber!="")
			$this->db->limit($limit,($pagenumber*$limit)-$limit);
		else
			$this->db->limit($limit,$start);

			$this->db->join('kelas','kelas.id_kelas = absensi_siswa.id_kelas');
			$this->db->join('tahun_ajaran','tahun_ajaran.id_tahun_ajaran = kelas.id_tahun_ajaran');
			$this->db->join('user','user.id_user = absensi_siswa.id_user_insert');
			$this->db->order_by('date_absensi','DESC');
			$this->db->group_by('date_absensi');
			$this->db->group_by('absensi_siswa.id_kelas');
		$query = $this->db->get('absensi_siswa');
		if($query->num_rows()>0){
			return $query->result();
		}
		else return FALSE;
	}
	function fetchAbsensiStudent($id_kelas,$date) {
		$this->db->join('kelas','kelas.id_kelas = absensi_siswa.id_kelas');
		$this->db->join('siswa','siswa.id_siswa = absensi_siswa.id_siswa');
		$this->db->join('user','user.id_user = absensi_siswa.id_user_insert');
		$this->db->order_by('date_absensi','DESC');
		$this->db->where('absensi_siswa.id_kelas',$id_kelas);
		$this->db->where('absensi_siswa.date_absensi',$date);
		$this->db->order_by('nama_siswa','ASC');
		$query = $this->db->get('absensi_siswa');
		if($query->num_rows()>0){
			return $query->result();
		}
		else return FALSE;
	}
	function fetchNisSiswa($id_kelas) {
		$kelas = $this->mod->getDataWhere('kelas','id_kelas',$id_kelas);
		$this->db->join('tahun_ajaran','tahun_ajaran.id_tahun_ajaran = siswa.id_tahun_masuk');
		$this->db->join('user','user.id_user = siswa.id_user_input');
		$this->db->where('status_siswa',0);
		$this->db->where('as_siswa.id_siswa NOT IN (SELECT as_siswa_kelas.id_siswa from as_siswa_kelas where id_kelas = \''.$id_kelas.'\')');
		$this->db->order_by('date_input','DESC');
		$query = $this->db->get('siswa');
		if($query->num_rows()>0){
			return $query->result();
		}
		else return FALSE;
	}
}
